/**
 * Created by Sebastian on 02.04.2016.
 */

'use strict';

cddApp.controller('mainController', ['$scope', 'dataService', function($scope, dataService) {

    $scope.docData = dataService.getAllData();

}]);