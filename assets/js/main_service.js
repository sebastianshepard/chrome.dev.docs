/**
 * Created by Sebastian on 02.04.2016.
 */

'use strict';

cddApp.service('dataService', ['$http', function($http) {

    this.getAllData = function() {

        var path_data = '/assets/js/data/';
        var data_files = ['official', 'jsAPI', 'thirdParty'];
        var json_data = [];

        for(var i in data_files) {
            $http( {
                method: "POST",
                url: path_data + data_files[i] + '.json'
            }).then(function(response) {
                json_data.push(response.data);
            });
        }

        return json_data;
    };

}]);